package br.com.ac.aljugtv;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class ImageAdapter extends BaseAdapter {
    private Context mContext;

    public ImageAdapter(Context c) {
        mContext = c;
    }

    @Override
    public int getCount() {
        return mThumbIds.length;
    }

    @Override
    public Object getItem(int position) {
        return mThumbNames[position].toString();
    }

    public String getPathVideo(int position){
        return mThumbPaths[position].toString();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {  // if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(85, 85));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(8, 8, 8, 8);
        } else {
            imageView = (ImageView) convertView;
        }

        imageView.setImageResource(mThumbIds[position]);
        return imageView;
    }

    // references to our images
    private Integer[] mThumbIds = {
            R.drawable.abc, R.drawable.amazon, R.drawable.bbc,
            R.drawable.bing_alt, R.drawable.chicagobulls, R.drawable.ebay,
            R.drawable.fox, R.drawable.itv, R.drawable.microsoft_alt, R.drawable.my_cnn,
            R.drawable.nbaive, R.drawable.nbc, R.drawable.nnetflix, R.drawable.rrdio_alt,
            R.drawable.tf1, R.drawable.yahooo_alt, R.drawable.you_tube, R.drawable.yahoo

    };

    // references to our image names
    private String[] mThumbNames = {
            "ABC", "Amazon", "BBC",
            "BING", "CHICAGO BULLS", "EBAY",
            "FOX", "ITV", "MICORSOFT", "CNN",
            "NBA LIVE", "NBC", "NETFILX", "RDIO",
            "TFL1", "YAHOO!", "YOUTUBE", "YAHOO! CHANNEL"

    };

    // references to our image names
    private String[] mThumbPaths = {
            "google_tv", "google_tv", "bbc",
            "google_tv", "google_tv", "google_tv",
            "google_tv", "google_tv", "google_tv", "google_tv",
            "google_tv", "google_tv", "google_tv", "google_tv",
            "google_tv", "google_tv!", "google_tv", "google_tv"

    };

}
