package br.com.ac.aljugtv;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.VideoView;

public class MainActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        GridView gridview = (GridView) findViewById(R.id.gridview);
        final ImageAdapter imageAdapter = new ImageAdapter(this);
        gridview.setAdapter(imageAdapter);

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                final ProgressDialog progressDialog = ProgressDialog.show(MainActivity.this, "",
                        "Loading " + imageAdapter.getItem(position) + " Please wait...", true);

                final String valueURIVideo = imageAdapter.getPathVideo(position);
                final String channel = imageAdapter.getItem(position).toString();

                final Handler h = new Handler() {
                    @Override
                    public void handleMessage(Message message) {
                        progressDialog.dismiss();

                        Uri uri = Uri.parse("android.resource://br.com.ac.aljugtv/raw/"+valueURIVideo);

                        Dialog d = new Dialog(MainActivity.this);

                        d.setTitle(channel);

                        View v = LayoutInflater.from(MainActivity.this).inflate(R.layout.dialog, null);

                        VideoView videoView = (VideoView) v.findViewById(R.id.videoView);
                        videoView.setVideoURI(uri);

                        d.setContentView(v);
                        d.show();

                        videoView.start();


                    }
                };
                h.sendMessageDelayed(new Message(), 2000);

            }
        });

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {


        switch (keyCode) {
            case KeyEvent.KEYCODE_MEDIA_STOP:

                Log.d("cesar", "Pausing the slideshow");

                return true;

            case KeyEvent.KEYCODE_MEDIA_PLAY:

                Log.d("cesar", "Resuming the slideshow");

                return true;

                /*
                 * Keycodes should always be passed upwards in the chain for handling.
                 */
            default:
                return super.onKeyDown(keyCode, event);
        }
    }

}
